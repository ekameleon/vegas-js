'use strict' ;

import './ioc/MagicReference'
import './ioc/ObjectArgument'
import './ioc/ObjectAttribute'
import './ioc/ObjectConfig'
import './ioc/ObjectDefinitionContainer'
import './ioc/ObjectFactory'
import './ioc/ObjectListener'
import './ioc/ObjectOrder'
import './ioc/ObjectScope'
import './ioc/ObjectStrategies'