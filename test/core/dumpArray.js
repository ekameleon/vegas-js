"use strict" ;

import dumpArray from 'core/dumpArray.js' ;

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.dumpArray' , () =>
{
    it('dumpArray([]) === []' , () => { assert.equal( dumpArray([]) , "[]" ); });

    it('dumpArray([1,2,3]) === "[1,2,3]"' , () => { assert.equal( dumpArray([1,2,3]) , "[1,2,3]" ); });

    it('dumpArray([1,2,3],true) === "\\n[\\n1,\\n2,\\n3]"' , () =>
    {
        let exp =
`
[
    1,
    2,
    3
]`;
        assert.equal( dumpArray([1,2,3],true) , exp );
    });
});
