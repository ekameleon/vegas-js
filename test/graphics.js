'use strict' ;

import './graphics/Align'
import './graphics/ArcType'
import './graphics/Border'
import './graphics/CardinalDirection'
import './graphics/Corner'
import './graphics/Direction'
import './graphics/Directionable'
import './graphics/DirectionOrder'
import './graphics/FillStyle'
import './graphics/Layout'
import './graphics/LayoutBufferMode'
import './graphics/LineStyle'
import './graphics/Orientation'
import './graphics/Position'
import './graphics/ZOrder'

import './graphics/colors'
import './graphics/geom'
import './graphics/tmx'

// import chai from 'chai' ;
//
// import { graphics } from '../src/graphics' ;
//
// const assert = chai.assert ;
//
// describe( 'graphics' , () =>
// {
//     it('graphics.isDirectionable !== null', () =>
//     {
//         assert.isNotNull( graphics.isDirectionable );
//     });
//
//     it('graphics.Align is not null', () =>
//     {
//         assert.isNotNull( graphics.Align );
//     });
//     it('graphics.ArcType !== null', () =>
//     {
//         assert.isNotNull( graphics.ArcType );
//     });
//     it('graphics.Border !== null', () =>
//     {
//         assert.isNotNull( graphics.Border );
//     });
//     it('graphics.CardinalDirection !== null', () =>
//     {
//         assert.isNotNull( graphics.CardinalDirection );
//     });
//     it('graphics.Corner !== null', () =>
//     {
//         assert.isNotNull( graphics.Corner );
//     });
//     it('graphics.Direction !== null', () =>
//     {
//         assert.isNotNull( graphics.Direction );
//     });
//     it('graphics.Directionable !== null', () =>
//     {
//         assert.isNotNull( graphics.Direction );
//     });
//     it('graphics.DirectionOrder !== null', () =>
//     {
//         assert.isNotNull( graphics.DirectionOrder );
//     });
//     it('graphics.Orientation !== null', () =>
//     {
//         assert.isNotNull( graphics.Orientation );
//     });
//     it('graphics.Position !== null', () =>
//     {
//         assert.isNotNull( graphics.Position );
//     });
//     it('graphics.ZOrder !== null', () =>
//     {
//         assert.isNotNull( graphics.ZOrder );
//     });
// });