'use strict' ;

// CHAI Doc : http://chaijs.com/api/assert/

import './performance'
//import './version' // FIXME
import './core'
import './system'

import './graphics'

import './mocks.js';
