"use strict" ;

/**
 * This interface defines a graphic object or component with a direction.
 * @summary This interface defines a graphic object or component with a direction.
 * @name Directionable
 * @memberof graphics
 * @interface
 */
export default function Directionable()
{
    /**
     * The direction of the object.
     * @name direction
     * @memberof graphics.Directionable
     * @instance
     * @default null
     */
    this.direction = null ;
}

Directionable.prototype = Object.create( Object.prototype );
Directionable.prototype.constructor = Directionable;