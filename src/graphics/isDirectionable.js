"use strict" ;

import Directionable from './Directionable'

/**
 * Indicates if the specific object is {@link graphics.Directionable} and contains the <code>direction</code> property.
 * @name isDirectionable
 * @function
 * @instance
 * @memberof graphics
 * @param {object} target - The object to evaluate.
 * @return <code>true</code> if the object is {@link graphics.Directionable}.
 */
export default function isDirectionable( target )
{
    if( target )
    {
        return (target instanceof Directionable) || ('direction' in target) ;
    }
    return false ;
}