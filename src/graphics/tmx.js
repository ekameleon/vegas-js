"use strict" ;

import '../polyfill/Object'

import Base from './tmx/Base'
import Frame from './tmx/Frame'
import Image from './tmx/Image'
import Layer from './tmx/Layer'
import Property from './tmx/Property'
import PropertyType from './tmx/PropertyType'
import Terrain from './tmx/Terrain'
import Tile from './tmx/Tile'
import TileLayer from './tmx/TileLayer'
import TileMap from './tmx/TileMap'
import TileMapOrientation from './tmx/TileMapOrientation'
import TileMapRenderOrder from './tmx/TileMapRenderOrder'
import TileObject from './tmx/TileObject'
import TileSet from './tmx/TileSet'

/**
 * The {@link graphics.tmx} library is a set of classes and utilities for Tile Maps Operations. Based on the TMX (Tile Map XML) map format used by <b>Tiled</b> is a flexible way to describe a tile based map. It can describe maps with any tile size, any amount of layers, any number of tile sets and it allows custom properties to be set on most elements. Beside tile layers, it can also contain groups of objects that can be placed freely.
 * @summary The {@link graphics.tmx} library is a set of classes and utilities for GTile Maps Operations.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace graphics.tmx
 * @memberof graphics
 */
export var tmx = Object.assign
({
    Base : Base ,
    Frame : Frame ,
    Image : Image ,
    Layer : Layer ,
    Property : Property ,
    PropertyType : PropertyType ,
    Terrain : Terrain ,
    Tile : Tile ,
    TileLayer : TileLayer ,
    TileMap : TileMap ,
    TileMapOrientation : TileMapOrientation ,
    TileMapRenderOrder : TileMapRenderOrder ,
    TileObject : TileObject ,
    TileSet : TileSet
}) ;