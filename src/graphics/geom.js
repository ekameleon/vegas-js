"use strict" ;

import '../polyfill/Object'

import AspectRatio    from './geom/AspectRatio'
import Circle         from './geom/Circle'
import ColorTransform from './geom/ColorTransform'
import Dimension      from './geom/Dimension'
import EdgeMetrics    from './geom/EdgeMetrics'
import Ellipse        from './geom/Ellipse'
import Matrix         from './geom/Matrix'
import Point          from './geom/Point'
import Polygon        from './geom/Polygon'
import Rectangle      from './geom/Rectangle'
import Vector2D       from './geom/Vector2D'
import Vector3D       from './geom/Vector3D'

/**
 * The {@link graphics.geom} library is a set of classes and utilities for Geometry Operations.
 * @summary The {@link graphics.geom} library is a set of classes and utilities for Geometry Operations.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace graphics.geom
 * @memberof graphics
 */
const geom = Object.assign
({
    AspectRatio    : AspectRatio ,
    Circle         : Circle ,
    ColorTransform : ColorTransform ,
    Dimension      : Dimension ,
    EdgeMetrics    : EdgeMetrics ,
    Ellipse        : Ellipse ,
    Matrix         : Matrix ,
    Point          : Point ,
    Polygon        : Polygon ,
    Rectangle      : Rectangle ,
    Vector2D       : Vector2D ,
    Vector3D       : Vector3D
}) ;

export default geom ;