"use strict" ;

import './polyfill/Object' ;

import isDirectionable from './graphics/isDirectionable'
import isMeasurable    from './graphics/isMeasurable'

import Align              from './graphics/Align'
import ArcType            from './graphics/ArcType'
import Border             from './graphics/Border'
import CardinalDirection  from './graphics/CardinalDirection'
import Corner             from './graphics/Corner'
import Direction          from './graphics/Direction'
import Directionable      from './graphics/Directionable'
import DirectionOrder     from './graphics/DirectionOrder'
import FillStyle          from './graphics/FillStyle'

import Layout             from './graphics/Layout'
import LayoutBufferMode   from './graphics/LayoutBufferMode'
import LayoutEntry        from './graphics/LayoutEntry'
import LineStyle          from './graphics/LineStyle'
import Orientation        from './graphics/Orientation'
import Position           from './graphics/Position'
import ZOrder             from './graphics/ZOrder'

import colors  from './graphics/colors'
import display from './graphics/display'
import geom    from './graphics/geom'

/**
 * The {@link graphics} package is an intuitive graphics API to manipulate all display objects in your applications. Offers a lot of powerful functionality to create and work with graphics, colors and geometrics objects, all neatly wrapped up in a well designed, consistent and clean programming interface.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace graphics
 * @version 1.0.19
 * @since 1.0.7
 */
const graphics = Object.assign
({
    // ----- Singletons

    isDirectionable : isDirectionable ,

    // ----- Classes and enumerations

    Align              : Align ,
    ArcType            : ArcType ,
    Border             : Border ,
    CardinalDirection  : CardinalDirection ,
    Corner             : Corner ,
    Direction          : Direction ,
    Directionable      : Directionable ,
    DirectionOrder     : DirectionOrder ,
    FillStyle          : FillStyle ,
    isMeasurable       : isMeasurable ,
    Layout             : Layout ,
    LayoutBufferMode   : LayoutBufferMode ,
    LayoutEntry        : LayoutEntry ,
    LineStyle          : LineStyle ,
    Orientation        : Orientation ,
    Position           : Position ,
    ZOrder             : ZOrder ,

    // ----- packages

    colors  : colors ,
    display : display ,
    geom    : geom
}) ;

export default graphics ;