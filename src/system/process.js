"use strict" ;

import '../polyfill/Object'

import Action        from './process/Action'
import ActionEntry   from './process/ActionEntry'
import Apply         from './process/Apply'
import Batch         from './process/Batch'
import BatchTask     from './process/BatchTask'
import Cache         from './process/Cache'
import Call          from './process/Call'
import Chain         from './process/Chain'
import Do            from './process/Do'
import FrameTimer    from './process/FrameTimer'
import Lock          from './process/Lock'
import Lockable      from './process/Lockable'
import Priority      from './process/Priority'
import Resetable     from './process/Resetable'
import Resumable     from './process/Resumable'
import Runnable      from './process/Runnable'
import Startable     from './process/Startable'
import Stoppable     from './process/Stoppable'
import Task          from './process/Task'
import TaskGroup     from './process/TaskGroup'
import TaskPhase     from './process/TaskPhase'
import TimeoutPolicy from './process/TimeoutPolicy'
import Timer         from './process/Timer'
import Unlock        from './process/Unlock'

import isLockable  from './process/isLockable'
import isResetable from './process/isResetable'
import isResumable from './process/isResumable'
import isRunnable  from './process/isRunnable'
import isStartable from './process/isStartable'
import isStoppable from './process/isStoppable'

/**
 * The {@link system.process} library allow you to create and manage asynchronous operations in your applications.
 * @summary The {@link system.process} library allow you to create and manage asynchronous operations in your applications.
 * @namespace system.process
 * @memberof system
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 */
const process = Object.assign
({
    isLockable    : isLockable ,
    isResetable   : isResetable ,
    isResumable   : isResumable ,
    isRunnable    : isRunnable ,
    isStartable   : isStartable ,
    isStoppable   : isStoppable ,

    Action        : Action ,
    ActionEntry   : ActionEntry ,
    Apply         : Apply ,
    Batch         : Batch ,
    BatchTask     : BatchTask ,
    Cache         : Cache ,
    Call          : Call ,
    Chain         : Chain ,
    Do            : Do,
    FrameTimer    : FrameTimer,
    Lock          : Lock ,
    Lockable      : Lockable ,
    Priority      : Priority ,
    Resetable     : Resetable ,
    Resumable     : Resumable ,
    Runnable      : Runnable ,
    Startable     : Startable ,
    Stoppable     : Stoppable ,
    Task          : Task ,
    TaskGroup     : TaskGroup ,
    TaskPhase     : TaskPhase ,
    TimeoutPolicy : TimeoutPolicy,
    Timer         : Timer,
    Unlock        : Unlock
}) ;

export default process ;