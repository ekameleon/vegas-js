/*jshint unused: false*/


"use strict" ;

/**
 * An Evaluable class can interpret an object to another object.
 * <p>It's not necessary a parser, but the most common cases would be a string being evaluated to an object structure.</p>
 * <p><b>Note:</b> eval always take one and only one argument, if you need to configure the evaluator pass different arguments in the constructor.</p>
 * @name Evaluable
 * @memberof system
 * @interface
 */
class Evaluable
{
    // noinspection JSAnnotator
    
    /**
     * Evaluates the specified object.
     * @param {*} value - The object to evaluates.
     * @return The result of the evaluation.
     * @name eval
     * @memberof system.Evaluable
     * @function
     * @instance
     */
    eval( value )
    {
    
    }

    /**
     * Returns the string representation of this instance.
     * @return {string} The string representation of this instance.
     * @name toString
     * @memberof system.Evaluable
     * @instance
     * @function
     */
    toString()
    {
        return '[' + this.constructor.name + ']' ;
    }
}

export default Evaluable;