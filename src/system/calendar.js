"use strict" ;

import '../polyfill/Object.js' ;

import DayOfWeek  from './calendar/DayOfWeek.js' ;

/**
 * The {@link system.calendar} library provides a framework unified for manipulating calendar stufs.
 * @summary The {@link system.calendar} library provides a framework unified for manipulating calendar stufs.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace system.calendar
 * @memberof system
 */
const calendar = Object.assign
({
    DayOfWeek : DayOfWeek
}) ;

export default calendar ;