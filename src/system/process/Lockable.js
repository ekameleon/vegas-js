"use strict" ;

/**
 * This interface is implemented by all objects lockable.
 * @name Lockable
 * @memberof system.process
 * @interface
 */
export default function Lockable()
{
    Object.defineProperties( this ,
    {
        __lock__ : { writable : true  , value : false }
    }) ;
}

Lockable.prototype = Object.create( Object.prototype ,
{
    constructor : { writable : true , value : Lockable } ,

    /**
     * Returns <code>true</code> if the object is locked.
     * @return <code>true</code> if the object is locked.
     * @name isLocked
     * @memberof system.process.Lockable
     * @function
     * @instance
     */
    isLocked : { writable : true , value : function()
    {
        return this.__lock__ ;
    }},

    /**
     * Locks the object.
     * @name lock
     * @memberof system.process.Lockable
     * @function
     * @instance
     */
    lock : { writable : true , value : function()
    {
        this.__lock__ = true ;
    }},

    /**
     * Unlocks the object.
     * @name unlock
     * @memberof system.process.Lockable
     * @function
     * @instance
     */
    unlock : { writable : true , value : function()
    {
        this.__lock__ = false ;
    }},

    /**
     * The <code>toString()</code> method returns a string representing the object
     * @return A string representing the object.
     * @memberof system.transitions.Transition
     * @instance
     * @function
     */
    toString : { writable : true , value : function()
    {
        return '[' + this.constructor.name + ']' ;
    }}
});