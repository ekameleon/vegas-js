"use strict" ;

/**
 * This interface should be implemented by any class whose instances are intended to be started.
 * @name Startable
 * @memberof system.process
 * @interface
 */
export default function Startable() {}

Startable.prototype = Object.create( Object.prototype ,
{
    constructor : { writable : true , value : Startable } ,

    /**
     * Starts the command process.
     * @name start
     * @memberof system.process.Startable
     * @function
     * @instance
     */
    start : { writable : true , value : function(){} },

    /**
     * The <code>toString()</code> method returns a string representing the object
     * @return A string representing the object.
     * @memberof system.transitions.Transition
     * @instance
     * @function
     */
    toString : { writable : true , value : function()
    {
        return '[' + this.constructor.name + ']' ;
    }}
});