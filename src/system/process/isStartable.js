"use strict" ;

import Startable from './Startable'

/**
 * Indicates if the specific objet is {@link system.process.Startable|Startable} and contains a <code>start()</code> method.
 * @name isStartable
 * @function
 * @instance
 * @memberof system.process
 * @param {object} target - The object to evaluate.
 * @return <code>true</code> if the object is {@link system.process.Startable|Startable}.
 */
export default function isStartable( target )
{
    if( target )
    {
        if( target instanceof Startable )
        {
            return true ;
        }
        /*jshint -W069 */
        return Boolean( target['start'] ) && ( target.start instanceof Function ) ;
        /*jshint +W069 */
    }
    return false ;
}
