"use strict" ;

import Resumable from './Resumable'

/**
 * Indicates if the specific objet is {@link system.process.Resumable|Resumable} and contains a <code>resume()</code> method.
 * @name isResumable
 * @function
 * @instance
 * @memberof system.process
 * @param {object} target - The object to evaluate.
 * @return <code>true</code> if the object is {@link system.process.Resumable|Resumable}.
 */
export default function isResumable( target )
{
    if( target )
    {
        if( target instanceof Resumable )
        {
            return true ;
        }
        /*jshint -W069 */
        return Boolean( target['resume'] ) && ( target.resume instanceof Function ) ;
        /*jshint +W069 */
    }
    return false ;
}