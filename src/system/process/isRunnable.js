"use strict" ;

import Runnable from './Runnable'

/**
 * Indicates if the specific objet is {@link system.process.Runnable|Runnable} and contains a <code>run()</code> method.
 * @name isRunnable
 * @function
 * @instance
 * @memberof system.process
 * @param {object} target - The object to evaluate.
 * @return <code>true</code> if the object is {@link system.process.Runnable|Runnable}.
 */
export default function isRunnable( target )
{
    if( target )
    {
        if( target instanceof Runnable )
        {
            return true ;
        }
        /*jshint -W069 */
        return Boolean(target['run']) && ( target.run instanceof Function )  ;
        /*jshint +W069 */
    }
    return false ;
}