"use strict" ;

/**
 * The base interface for all <b>commands</b> in your applications. If only this interface is implemented by a command, it is treated as a synchronous command. For additional features like asynchronous execution, cancellation or suspension, several subinterfaces are available.
 * <p>In object-oriented programming, the command pattern is a behavioral design pattern in which an object is used to encapsulate all information needed to perform an action or trigger an event at a later time.</p>
 * This interface is used by all internal command executors and builders.
 * @name Runnable
 * @memberof system.process
 * @interface
 */

export default class Runnable
{
    /**
     * Run the command.
     * @name run
     * @memberof system.process.Runnable
     * @function
     * @instance
     */
    run()
    {
    
    }
    
    /**
     * The <code>toString()</code> method returns a string representing the object
     * @return A string representing the object.
     * @memberof system.transitions.Transition
     * @instance
     * @function
     */
    toString()
    {
        return '[' + this.constructor.name + ']' ;
    }
}