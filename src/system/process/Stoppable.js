"use strict" ;

/**
 * This interface should be implemented by any class whose instances are intended to be stopped.
 * @name Stoppable
 * @memberof system.process
 * @interface
 */
export default function Stoppable() {}

Stoppable.prototype = Object.create( Object.prototype ,
{
    constructor : { writable : true , value : Stoppable } ,

    /**
     * Stops the command process.
     * @name stop
     * @memberof system.process.Stoppable
     * @function
     * @instance
     */
    stop : { writable : true , value : function(){} },

    /**
     * The <code>toString()</code> method returns a string representing the object
     * @return A string representing the object.
     * @memberof system.transitions.Transition
     * @instance
     * @function
     */
    toString : { writable : true , value : function()
    {
        return '[' + this.constructor.name + ']' ;
    }}
});