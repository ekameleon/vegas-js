"use strict" ;

/**
 * This interface should be implemented by any class whose instances are intended to be resumed.
 * @name Resumable
 * @memberof system.process
 * @interface
 */
function Resumable() {}

Resumable.prototype = Object.create( Object.prototype ,
{
    constructor : { writable : true , value : Resumable } ,

    /**
     * Resumes the command process.
     * @name resume
     * @memberof system.process.Resumable
     * @function
     * @instance
     */
    resume : { writable : true , value : function(){} },

    /**
     * The <code>toString()</code> method returns a string representing the object
     * @return A string representing the object.
     * @memberof system.transitions.Transition
     * @instance
     * @function
     */
    toString : { writable : true , value : function()
    {
        return '[' + this.constructor.name + ']' ;
    }}
});

export default Resumable;