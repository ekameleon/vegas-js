"use strict" ;

/**
 * This interface should be implemented by any class whose instances are intended to be reseted.
 * @name Resetable
 * @memberof system.process
 * @interface
 */
export default function Resetable(){}

Resetable.prototype = Object.create( Object.prototype ,
{
    constructor : { writable : true , value : Resetable } ,

    /**
     * Resets the command process.
     * @name reset
     * @memberof system.process.Resetable
     * @function
     * @instance
     */
    reset : { writable : true , value : function(){} },

    /**
     * The <code>toString()</code> method returns a string representing the object
     * @return A string representing the object.
     * @memberof system.transitions.Transition
     * @instance
     * @function
     */
    toString : { writable : true , value : function()
    {
        return '[' + this.constructor.name + ']' ;
    }}
});