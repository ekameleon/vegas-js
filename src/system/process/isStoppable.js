"use strict" ;

import Stoppable from './Stoppable'

/**
 * Indicates if the specific objet is {@link system.process.Stoppable|Stoppable} and contains a <code>stop()</code> method.
 * @name isStoppable
 * @function
 * @instance
 * @memberof system.process
 * @param {object} target - The object to evaluate.
 * @return <code>true</code> if the object is {@link system.process.Stoppable|Stoppable}.
 */
export default function isStoppable( target )
{
    if( target )
    {
        if( target instanceof Stoppable )
        {
            return true ;
        }
        /*jshint -W069 */
        return Boolean( target['stop'] ) && ( target.stop instanceof Function ) ;
        /*jshint +W069 */
    }
    return false ;
}