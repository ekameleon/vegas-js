"use strict" ;

import Lockable from './Lockable'

/**
 * Indicates if the specific objet is {@link system.process.Lockable|Lockable} or contains the <code>lock()</code> / <code>unlock()</code> / <code>isLocked()</code> methods.
 * @name isLockable
 * @memberof system.process
 * @function
 * @instance
 * @param {object} target - The object to evaluate.
 * @return <code>true</code> if the object is {@link system.process.Lockable|Lockable}.
 */
export default function isLockable( target )
{
    if( target )
    {
        if( target instanceof Lockable )
        {
            return true ;
        }
        else
        {
            /*jshint -W069 */
            return Boolean( target['isLocked'] ) && ( target.isLocked instanceof Function ) &&
                   Boolean( target['lock'] )     && ( target.lock     instanceof Function ) &&
                   Boolean( target['unlock'] )   && ( target.unlock   instanceof Function ) ;
            /*jshint +W069 */
        }
    }
    return false ;
}