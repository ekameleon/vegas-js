"use strict" ;

import performance from '../../polyfill/performance'
import Receiver  from '../signals/Receiver'
import Motion    from './Motion'

/**
 * The internal MotionNextFrame Receiver.
 * @name MotionNextFrame
 * @memberof system.transitions
 * @constructs
 * @class
 * @private
 * @implements Receiver
 * @param {system.transitions.Motion} motion - The Motion reference who emit the messages.
 */
export default function MotionNextFrame( motion )
{
    this.motion = motion instanceof Motion ? motion : null ;
}

MotionNextFrame.prototype = Object.create( Receiver.prototype ,
{
    constructor : { value : MotionNextFrame } ,

    /**
     * Receives the signal message.
     * @name receive
     * @memberof system.transitions.MotionNextFrame
     * @function
     * @instance
     */
    receive : { value : function()
    {
        if( this.motion )
        {
            this.motion.setTime( (this.motion.useSeconds) ? ( ( performance.now() - this.motion._startTime ) / 1000 ) : ( this.motion._time + 1 ) ) ;
        }
    }}
}) ;