"use strict" ;

import '../polyfill/Object'

import PRNG from './numeric/PRNG'
import Range from './numeric/Range'
import RomanNumber from './numeric/RomanNumber'

/**
 * The {@link system.numeric} library contains classes and tools that provides extra <code>numeric</code> methods and implementations.
 * @summary The {@link system.numeric} library contains classes and tools that provides extra <code>numeric</code> methods and implementations.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace system.numeric
 * @memberof system
 */
const numeric = Object.assign
({
    PRNG  : PRNG ,
    Range : Range ,
    RomanNumber : RomanNumber
}) ;

export default numeric ;