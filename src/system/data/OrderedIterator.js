/*jshint unused: false*/
"use strict" ;

import Iterator from './Iterator'

/**
 * Defines an iterator that operates over an ordered collection. This iterator allows both forward and reverse iteration through the collection.
 * @name OrderedIterator
 * @extends system.data.Iterator
 * @interface
 * @memberof system.data
 */
export default function OrderedIterator() {}

OrderedIterator.prototype = Object.create( Iterator.prototype ) ;
OrderedIterator.prototype.constructor = OrderedIterator ;

/**
 * Checks to see if there is a previous element that can be iterated to.
 * @memberof system.data.OrderedIterator
 * @function
 * @instance
 */
OrderedIterator.prototype.hasPrevious = function() {}

/**
 * Returns the previous element in the collection.
 * @return the previous element in the collection.
 * @memberof system.data.OrderedIterator
 * @function
 * @instance
 */
OrderedIterator.prototype.previous = function() {}

/**
 * Returns the string representation of this instance.
 * @return the string representation of this instance.
 * @name toString
 * @memberof system.data.OrderedIterator
 * @function
 * @instance
 */
OrderedIterator.prototype.toString = function () { return '[OrderedIterator]' ; }