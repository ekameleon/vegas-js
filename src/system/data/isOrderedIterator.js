/*jshint unused: false*/
"use strict" ;

import OrderedIterator from './OrderedIterator'

/**
 * Indicates if the specific objet is an {@link system.data.OrderedIterator|OrderedIterator}.
 * @name isOrderedIterator
 * @memberof system.data
 * @function
 * @param {object} target - The target object to evaluate.
 * @return <code>true</code> if the object is an {@link system.data.OrderedIterator|OrderedIterator}.
 */
export default function isOrderedIterator( target )
{
    var bool = false ;
    if( target )
    {
        bool =
        (
            (target instanceof OrderedIterator) ||
            (
                (('hasNext'     in target) && (target.hasNext     instanceof Function)) &&
                (('hasPrevious' in target) && (target.hasPrevious instanceof Function)) &&
                (('key'         in target) && (target.key         instanceof Function)) &&
                (('next'        in target) && (target.next        instanceof Function)) &&
                (('previous'    in target) && (target.previous    instanceof Function)) &&
                (('remove'      in target) && (target.remove      instanceof Function)) &&
                (('reset'       in target) && (target.reset       instanceof Function)) &&
                (('seek'        in target) && (target.seek        instanceof Function))
            )
        );
    }
    return bool ;
}