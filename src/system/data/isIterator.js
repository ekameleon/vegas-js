/*jshint unused: false*/
"use strict" ;

import Iterator from './Iterator'

/**
 * Indicates if the specific object is an {@link system.data.Iterator|Iterator}.
 * @name isIterator
 * @memberof system.data
 * @function
 * @param {object} target - The target object to evaluate.
 * @return A <code>true</code> value if the object is an {@link system.data.Iterator|Iterator}.
 */
export default function isIterator( target )
{
    if( target )
    {
        return (target instanceof Iterator) ||
               (
                    /*jshint -W069 */
                    (Boolean(target['delete'])  && (target.delete  instanceof Function)) &&
                    (Boolean(target['hasNext']) && (target.hasNext instanceof Function)) &&
                    (Boolean(target['key'])     && (target.key     instanceof Function)) &&
                    (Boolean(target['next'])    && (target.next    instanceof Function)) &&
                    (Boolean(target['reset'])   && (target.reset   instanceof Function)) &&
                    (Boolean(target['seek'])    && (target.seek    instanceof Function))
                    /*jshint +W069 */
                );
    }
    return false ;
}