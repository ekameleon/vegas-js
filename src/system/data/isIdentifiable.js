"use strict" ;

import Identifiable from './Identifiable'

/**
 * Indicates if the specific <code>target</code> is an {@link system.data.Identifiable|Identifiable} object.
 * @name isIdentifiable
 * @memberof system.data
 * @function
 * @param {object} target - The object to evaluate.
 * @return A <code>true</code> value if the object is an {@link system.data.Identifiable|Identifiable} object.
 */
export default function isIdentifiable( target )
{
    if( target )
    {
        return (target instanceof Identifiable) || ('id' in target) ;
    }
    return false ;
}