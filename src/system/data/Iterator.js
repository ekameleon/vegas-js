/*jshint unused: false*/
"use strict" ;

/**
 * This interface defines the iterator pattern over a collection.
 * <p><b>Implementors:</b>
 * <ul>
 * <li>{@link system.data.iterators.ArrayIterator|ArrayIterator}</li>
 * <li>{@link system.data.iterators.MapIterator|MapIterator}</li>
 * </ul>
 * </p>
 * @name Iterator
 * @interface
 * @memberof system.data
 */
export default function Iterator() {}

Iterator.prototype = Object.create( Object.prototype ,
{
    constructor : { writable : true , value : Iterator } ,

    /**
     * Deletes from the underlying collection the last element returned by the iterator (optional operation).
     * @name delete
     * @memberof system.data.Iterator
     * @instance
     * @function
     */
    delete : { writable : true , value : function() {} } ,

    /**
     * Returns <code>true</code> if the iteration has more elements.
     * @return A <code>true</code> value if the iteration has more elements.
     * @name hasNext
     * @memberof system.data.Iterator
     * @instance
     * @function
     */
    hasNext : { writable : true , value : function() {} } ,

    /**
     * Returns the current key of the internal pointer of the iterator (optional operation).
     * @return the current key of the internal pointer of the iterator (optional operation).
     * @memberof system.data.Iterator
     * @name key
     * @memberof system.data.Iterator
     * @instance
     * @function
     */
    key : { writable : true , value : function() {} } ,

    /**
     * Returns the next element in the iteration.
     * @return the next element in the iteration.
     * @name next
     * @memberof system.data.Iterator
     * @instance
     * @function
     */
    next : { writable : true , value : function() {} } ,

    /**
     * Reset the internal pointer of the iterator (optional operation).
     * @memberof system.data.Iterator
     * @name reset
     * @instance
     * @function
     */
    reset : { writable : true , value : function() {} } ,

    /**
     * Changes the position of the internal pointer of the iterator (optional operation).
     * @name seek
     * @memberof system.data.Iterator
     * @instance
     * @function
     */
    seek : { writable : true , value : function ( position ) {} } ,

    /**
     * Returns the string representation of this instance.
     * @return the string representation of this instance.
     * @name toString
     * @memberof system.data.Iterator
     * @instance
     * @function
     */
    toString :
    {
        writable : true , value : function ()
        {
            return '[' + this.constructor.name + ']' ;
        }
    }
}) ;
