/*jshint unused: false*/
"use strict" ;

import Evaluable from './Evaluable'

/**
 * Indicates if the specific objet is {@link system.Evaluable|Evaluable}.
 * @name isEvaluable
 * @function
 * @memberof system
 * @param {object} target - The object to evaluate.
 * @return <code>true</code> if the object is {@link system.Evaluable|Evaluable}.
 */
export default function isEvaluable( target )
{
    if( target )
    {
        return (target instanceof Evaluable) ||
               (( 'eval' in target ) && ( target.eval instanceof Function ))  ;
    }
    return false ;
}
