"use strict" ;

import Rule from './Rule'

/**
 * Indicates if the specific object is a {@link system.rules.Rule} object and contains an <code>eval</code> method.
 * @memberof system.rules
 * @function
 * @param {Object} target - The object to validate.
 * @return A <code>true</code> value if the object is a {@link system.rules.Rule} instance.
 */
export default function isRule( target )
{
    if( target )
    {
        return (target instanceof Rule) || (( 'eval' in target ) && ( target['eval'] instanceof Function ))  ;
    }
    return false ;
}