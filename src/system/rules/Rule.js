"use strict"

/**
 * Defines the rule to evaluate a basic or complex condition.
 * @name Rule
 * @memberof system.rules
 * @interface
 */
export default function Rule() {}

Rule.prototype = Object.create( Object.prototype ,
{
    constructor : { writable : true , value : Rule } ,

    /**
     * Evaluates the specified condition.
     * @name eval
     * @memberof system.rules.Rule
     * @function
     * @instance
     */
    eval : { writable : true , value : function()
    {
        //
    }},

    /**
     * Returns the string representation of this instance.
     * @return the string representation of this instance.
     * @name toString
     * @memberof system.rules.Rule
     * @function
     * @instance
     */
    toString : { value : function()
    {
        return '[' + this.constructor.name + ']' ;
    }}
});