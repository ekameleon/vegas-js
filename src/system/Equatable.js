/*jshint unused: false*/
"use strict" ;

/**
 * This interface is implemented by classes that can compare an object with their objects.
 * @name Equatable
 * @memberof system
 * @interface
 */
export default function Equatable()
{

}

Equatable.prototype = Object.create( Object.prototype );
Equatable.prototype.constructor = Equatable;

/**
 * Compares the specified object with this object for equality.
 * @memberof system.Equatable
 * @function
 * @param {*} object - The object to evaluates.
 * @return {boolean} true if the the specified object is <b>equal to</b> this object.
 */
Equatable.prototype.equals = function( object )
{
    //
}