/*jshint unused: false*/
"use strict" ;

import Equatable from './Equatable'

/**
 * Indicates if the specific objet is Equatable.
 * @function
 * @memberof system
 * @param {object} target - The object to evaluate.
 * @return <code>true</code> if the object is {@link system.Equatable|Equatable}.
 */
export default function isEquatable( target )
{
    if( target )
    {
        return ( target.equals && ( target.equals instanceof Function ) ) || (target instanceof Equatable)   ;
    }

    return false ;
}