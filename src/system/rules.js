"use strict" ;

import '../polyfill/Object'

import And                 from './rules/And'
import BooleanRule         from './rules/BooleanRule'
import DivBy               from './rules/DivBy'
import EmptyString         from './rules/EmptyString'
import Equals              from './rules/Equals'
import Even                from './rules/Even'
import False               from './rules/False'
import GreaterOrEqualsThan from './rules/GreaterOrEqualsThan'
import GreaterThan         from './rules/GreaterThan'
import IsBoolean           from './rules/IsBoolean'
import IsNaN               from './rules/IsNaN'
import IsNumber            from './rules/IsNumber'
import IsString            from './rules/IsString'
import LessOrEqualsThan    from './rules/LessOrEqualsThan'
import LessThan            from './rules/LessThan'
import Not                 from './rules/Not'
import NotEquals           from './rules/NotEquals'
import Null                from './rules/Null'
import Odd                 from './rules/Odd'
import Or                  from './rules/Or'
import Rule                from './rules/Rule'
import True                from './rules/True'
import Undefined           from './rules/Undefined'
import Zero                from './rules/Zero'

import isRule from './rules/isRule'

/**
 * The {@link system.rules} library defines a set of functions and classes to evaluate some basic or complex conditions in your applications.
 * @summary The {@link system.rules} library defines a set of functions and classes to evaluate some basic or complex conditions in your applications.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace system.rules
 * @memberof system
 */
const rules = Object.assign
({
    // singletons
    isRule : isRule ,

    // classes
    And                 : And ,
    BooleanRule         : BooleanRule ,
    DivBy               : DivBy ,
    EmptyString         : EmptyString ,
    Equals              : Equals ,
    Even                : Even ,
    False               : False ,
    GreaterOrEqualsThan : GreaterOrEqualsThan ,
    GreaterThan         : GreaterThan ,
    IsBoolean           : IsBoolean ,
    IsNaN               : IsNaN ,
    IsNumber            : IsNumber ,
    IsString            : IsString ,
    LessOrEqualsThan    : LessOrEqualsThan ,
    LessThan            : LessThan ,
    Odd                 : Odd ,
    Not                 : Not ,
    NotEquals           : NotEquals ,
    Null                : Null ,
    Or                  : Or ,
    Rule                : Rule,
    True                : True,
    Undefined           : Undefined,
    Zero                : Zero
}) ;

export default rules ;