/*jshint unused: false*/
"use strict" ;

import Formattable from './Formattable'

/**
 * Indicates if the specific objet is Formattable.
 * @name isFormattable
 * @function
 * @memberof system
 * @param {object} target - The object to evaluate.
 * @return <code>true</code> if the object is {@link system.Formattable|Formattable}.
 */
export default function isFormattable( target )
{
    if( target )
    {
        return (target instanceof Formattable) || (( 'format' in target ) && ( target.format instanceof Function ))  ;
    }

    return false ;
}