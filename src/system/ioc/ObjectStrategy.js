"use strict" ;

/**
 * Defines a custom factory strategy to build the objects.
 * @name ObjectStrategy
 * @class
 * @memberof system.ioc
 */
class ObjectStrategy
{
    /**
     * Returns the string representation of this instance.
     * @return the string representation of this instance.
     * @memberof system.ioc.ObjectStrategy
     * @function
     * @instance
     */
    toString()
    {
        return '[' + this.constructor.name + ']' ;
    }
}

export default ObjectStrategy;