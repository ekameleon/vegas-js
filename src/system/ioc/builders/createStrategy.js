"use strict" ;

import ObjectStrategies from '../ObjectStrategies'

import ObjectFactoryMethod         from '../strategies/ObjectFactoryMethod'
import ObjectFactoryProperty       from '../strategies/ObjectFactoryProperty'
import ObjectReference             from '../strategies/ObjectReference'
import ObjectStaticFactoryMethod   from '../strategies/ObjectStaticFactoryMethod'
import ObjectStaticFactoryProperty from '../strategies/ObjectStaticFactoryProperty'
import ObjectValue                 from '../strategies/ObjectValue'

/**
 * This helper create an <code>ObjectStrategy</code> object based on a specific generic object definition.
 * @memberof system.ioc.builders
 * @function
 * @private
 */
function createStrategy( o )
{
    if ( ObjectStrategies.FACTORY_METHOD in o )
    {
        return ObjectFactoryMethod.build( o[ ObjectStrategies.FACTORY_METHOD ] ) ;
    }
    else if ( ObjectStrategies.FACTORY_PROPERTY in o )
    {
        return ObjectFactoryProperty.build( o[ ObjectStrategies.FACTORY_PROPERTY ] ) ;
    }
    else if ( ObjectStrategies.FACTORY_REFERENCE in o )
    {
        return new ObjectReference( o[ ObjectStrategies.FACTORY_REFERENCE ] ) ;
    }
    else if ( ObjectStrategies.FACTORY_VALUE in o )
    {
        return new ObjectValue( o[ ObjectStrategies.FACTORY_VALUE ] ) ;
    }
    else if ( ObjectStrategies.STATIC_FACTORY_METHOD  in o )
    {
        return ObjectStaticFactoryMethod.build( o[ ObjectStrategies.STATIC_FACTORY_METHOD ] ) ;
    }
    else if ( ObjectStrategies.STATIC_FACTORY_PROPERTY in o )
    {
        return ObjectStaticFactoryProperty.build( o[ ObjectStrategies.STATIC_FACTORY_PROPERTY ] ) ;
    }
    else
    {
        return null ;
    }
}

export default createStrategy;