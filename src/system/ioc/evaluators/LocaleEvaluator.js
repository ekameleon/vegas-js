"use strict" ;

import ObjectConfig      from '../ObjectConfig'
import PropertyEvaluator from '../../evaluators/PropertyEvaluator'

/**
 * Evaluates a type string expression and return the value who corresponding in the config of the factory.
 * @memberof system.ioc.evaluators
 * @name LocaleEvaluator
 * @class
 * @private
 * @example
 * var i18n =
 * {
 *     message : "hello world" ,
 *     title   : "my title"    ,
 *     menu    :
 *     {
 *         title : "my menu title" ,
 *         label : "my label"
 *     }
 * }
 *
 * var configurator = new ObjectConfig() ;
 *
 * configurator.locale = i18n ;
 *
 * var evaluator = new LocaleEvaluator( configurator ) ;
 *
 * trace( evaluator.eval( "test"       ) ) ; // null
 * trace( evaluator.eval( "message"    ) ) ; // hello world
 * trace( evaluator.eval( "title"      ) ) ; // my title
 * trace( evaluator.eval( "menu.title" ) ) ; // my menu title
 * trace( evaluator.eval( "menu.label" ) ) ; // my label
 */
class LocaleEvaluator extends PropertyEvaluator
{
    constructor( config )
    {
        super() ;
        this.config = (config instanceof ObjectConfig) ? config : null ;
        Object.defineProperties( this ,
        {
            target : { get : function() { return this.config !== null ? this.config.locale : null ; } }
        }) ;
    }
}

export default LocaleEvaluator;