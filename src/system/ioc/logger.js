"use strict" ;

import Log from '../logging/Log' ;

/**
 * The {@link system.ioc} internal logger singleton with the channel <code>"system.ioc.logger"</code>.
 * @name logger
 * @memberof system.ioc
 * @type system.logging.Logger
 */
const logger = Log.getLogger( "system.ioc.logger" ) ;

export default logger ;