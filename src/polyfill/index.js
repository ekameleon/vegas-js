"use strict" ;

import './Date'
import './Function'
import './Math'
import './Object'
import './performance'
import './requestAnimationFrame'
import './Uint32Array'