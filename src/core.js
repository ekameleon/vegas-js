"use strict" ;

import './polyfill/Object'

import { global } from './core/global.js'

import dump  from   './core/dump'
import trace from   './core/trace'
import version from './core/version'

import { cancelAnimationFrame  } from './core/cancelAnimationFrame'
import { requestAnimationFrame } from './core/requestAnimationFrame'

import isBoolean from './core/isBoolean'
import isFloat   from './core/isFloat'
import isInt     from './core/isInt'
import isNumber  from './core/isNumber'
import isUint    from './core/isUint'
import isString  from './core/isString'

import arrays    from './core/arrays'
import chars     from './core/chars'
import colors    from './core/colors'
import date      from './core/date'
import dom       from './core/dom'
import easings   from './core/easings'
import functors  from './core/functors'
import maths     from './core/maths'
import numbers   from './core/numbers'
import objects   from './core/objects'
import random    from './core/random'
import reflect   from './core/reflect'
import strings   from './core/strings'

/**
 * The {@link core} package is specialized in functions utilities that are highly reusable without creating any dependencies : arrays, strings, chars, objects, numbers, maths, date, colors, etc.
 * <p>You can consider a library as a set of functions organized into classes, here with a <strong>"core"</strong> library in some cases we organize the functions in the package definitions without assembling them into a class.</p>
 * <p>Those functions are allowed to reuse the builtin types (Object, Array, Date, etc.), the Javascript API classes and packages, but nothing else.</p>
 * @summary The {@link core} package is specialized in functions utilities that are highly reusable without creating any dependencies.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/)|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace core
 * @version 1.0.19
 * @since 1.0.0
 */
const core = Object.assign
({
    global  : global ,
    dump    : dump ,
    trace   : trace ,
    version : version ,
    
    cancelAnimationFrame  : cancelAnimationFrame ,
    requestAnimationFrame : requestAnimationFrame ,

    isBoolean : isBoolean ,
    isFloat   : isFloat ,
    isInt     : isInt ,
    isNumber  : isNumber ,
    isString  : isString ,
    isUint    : isUint ,

    arrays   : arrays ,
    chars    : chars ,
    colors   : colors ,
    date     : date ,
    dom      : dom ,
    easings  : easings,
    functors : functors,
    maths    : maths ,
    numbers  : numbers,
    objects  : objects ,
    random   : random ,
    reflect  : reflect ,
    strings  : strings
}) ;

export default core ;