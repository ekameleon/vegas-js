/*jshint unused: false*/
"use strict" ;

/**
 * This interface defines an object groupable in the application.
 * @name Groupable
 * @memberof molecule
 * @interface
 */
export default class Groupable
{
    constructor()
    {
        Object.defineProperties( this ,
        {
            /**
             * Indicates with a boolean if this object is grouped.
             * @name group
             * @memberof molecule.Groupable
             * @default false
             * @type {boolean}
             * @instance
             */
            group : { value : false , configurable : true , writable : true } ,
        
            /**
             * Indicates the name of the group of this object.
             * @name groupName
             * @memberof molecule.Groupable
             * @default null
             * @type {string}
             * @instance
             */
            groupName : { value : null , configurable : true , writable : true }
        } );
    }
}