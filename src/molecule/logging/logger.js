"use strict" ;

import Log from '../../system/logging/Log'

/**
 * The molecule logger singleton constant.
 * @name logger
 * @memberof molecule.logging
 * @const
 */
const logger = Log.getLogger( 'molecule.logging.logger' ) ;

export default logger ;