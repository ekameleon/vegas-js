"use strict" ;

// --------- Dependencies

import draw  from './components/draw'
import label from './components/label'

// ---------  IOC Definitions

const components = [].concat
(
    { name : "draw"  , value : draw  } ,
    { name : "label" , value : label }
);

export default components ;

// components.forEach( ( component ) => AFRAME.registerComponent( component.name , component.value ) ) ;