"use strict" ;

import AEntity     from './display/AEntity'
import Assets      from './display/Assets'
import Box         from './display/Box'
import Button      from './display/Button'
import Circle      from './display/Circle'
import Cursor      from './display/Cursor'
import Cylinder    from './display/Cylinder'
import Image       from './display/Image'
import Material    from './display/Material'
import Plane       from './display/Plane'
import Ring        from './display/Ring'
import Scene       from './display/Scene'
import Sky         from './display/Sky'
import Sound       from './display/Sound'
import Sphere      from './display/Sphere'
import Text        from './display/Text'
import Videosphere from './display/Videosphere'

/**
 * The {@link molecule.render.aframe.display} package.
 * @summary The {@link molecule.render.aframe.display} package.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/)|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace molecule.render.aframe.display
 * @version 1.0.19
 * @since 1.0.8
 */
const display = Object.assign
({
    AEntity  : AEntity,
    Assets   : Assets,
    Box      : Box,
    Button   : Button,
    Circle   : Circle,
    Cursor   : Cursor,
    Cylinder : Cylinder,
    Image    : Image,
    Material : Material,
    Plane    : Plane,
    Ring     : Ring,
    Scene    : Scene,
    Sky      : Sky,
    Sound    : Sound,
    Sphere   : Sphere,
    Text     : Text,
    Videosphere : Videosphere
});

export default display ;