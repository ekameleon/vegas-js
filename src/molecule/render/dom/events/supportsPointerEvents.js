"use strict" ;

/**
 * Does the device support pointer events
 * https://www.w3.org/Submission/pointer-events/
 * @name supportsPointerEvents
 * @readonly
 * @member {boolean}
 * @memberof molecule.render.dom.events
 * @version 1.0.19
 * @since 1.0.8
 */
const supportsPointerEvents = !!window.PointerEvent;

export default supportsPointerEvents;