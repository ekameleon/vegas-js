"use strict" ;

/**
 * Does the device support touch events
 * https://www.w3.org/TR/touch-events/
 * @readonly
 * @member {boolean}
 * @memberof molecule.render.dom.events
 * @version 1.0.19
 * @since 1.0.8
 */
const supportsTouchEvents = 'ontouchstart' in window;

export default supportsTouchEvents;