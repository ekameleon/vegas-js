"use strict" ;

import '../../polyfill/Object'

import components from './pixi/components'
import display    from './pixi/display'
import layouts    from './pixi/layouts'
import process    from './pixi/process'
import rules      from './pixi/rules'

/**
 * The {@link molecule.render.pixi} library contains the rendering classes that the application uses the PIXI JS library to display 3D/VR elements.
 * @summary The {@link molecule.render.pixi} library contains the rendering classes that the application uses the PIXI JS library to display 3D/VR elements.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace molecule.render.pixi
 * @memberof molecule.render
 */
const pixi = Object.assign
({
    components : components,
    display    : display,
    layouts    : layouts,
    process    : process,
    rules      : rules
}) ;

export default pixi ;