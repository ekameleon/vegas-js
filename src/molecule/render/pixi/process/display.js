"use strict" ;
import AddChild from './display/AddChild'
import AddChildAt from './display/AddChildAt'
import AlignPivot from './display/AlignPivot'
import Hide from './display/Hide'
import IfContains from './display/IfContains'
import IfNotContains from './display/IfNotContains'
import InteractiveChildren from './display/InteractiveChildren'
import MoveTo from './display/MoveTo'
import RemoveChild from './display/RemoveChild'
import RemoveChildAt from './display/RemoveChildAt'
import RemoveChildren from './display/RemoveChildren'
import Show from './display/Show'
import SwapChildren from './display/SwapChildren'

/**
 * The {@link molecule.render.pixi.process.display} package.
 * @summary The {@link molecule.render.pixi.process.display} package.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/)|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace molecule.render.pixi.process.display
 * @memberof molecule.render.pixi.process
 * @version 1.0.19
 * @since 1.0.8
 */
const display = Object.assign
({
    AddChild : AddChild,
    AddChildAt : AddChildAt,
    AlignPivot : AlignPivot,
    Hide : Hide,
    IfContains : IfContains,
    IfNotContains : IfNotContains,
    InteractiveChildren : InteractiveChildren,
    MoveTo : MoveTo,
    RemoveChild : RemoveChild,
    RemoveChildAt : RemoveChildAt,
    RemoveChildren : RemoveChildren,
    Show : Show,
    SwapChildren : SwapChildren
});

export default display ;