"use strict" ;

import 'polyfill/Object'

import RadioButtonGroup from 'molecule/groups/RadioButtonGroup'

/**
 * The RadioButtonGroup singleton.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @memberof molecule.render.pixi.components.buttons
 * @static
 * @private
 * @version 1.0.19
 * @since 1.0.8
 */
const radio = new RadioButtonGroup() ;

export default radio ;