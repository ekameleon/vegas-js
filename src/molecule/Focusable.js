/*jshint unused: false*/
"use strict" ;

import Groupable   from './Groupable'
import isGroupable from './isGroupable'

/**
 * This interface defines an object groupable and focusable in the application.
 * @name Focusable
 * @memberof molecule
 * @interface
 */
export default class Focusable extends Groupable
{
    constructor()
    {
        super();
        Object.defineProperties( this ,
        {
            /**
             * A flag that indicates whether this control is selected.
             * @name selected
             * @memberof molecule.Focusable
             * @default false
             * @type {boolean}
             * @instance
             */
            selected : { value : false , configurable : true , writable : true }
        } );
    }
}