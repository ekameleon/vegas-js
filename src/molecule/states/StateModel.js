"use strict" ;

import MapModel from '../../system/models/maps/MapModel'

import State  from './State'

/**
 * The state model.
 * @summary Defines a state model.
 * @name StateModel
 * @class
 * @memberof molecule.states
 * @implements system.models.maps.MapModel
 * @constructs
 */
export default function StateModel()
{
    MapModel.call( this ) ;
}

StateModel.prototype = Object.create( MapModel.prototype ,
{
    constructor : { writable : true , value : StateModel } ,

    /**
     * Returns <code>true</code> if the specific value is valid.
     * @param {*} value - The value to check.
     * @return <code>true</code> if the specific value is valid.
     * @name supports
     * @memberof system.models.Model
     * @function
     * @instance
     */
    supports : { writable : true , value : function( value )
    {
        return value instanceof State ;
    }}
}) ;