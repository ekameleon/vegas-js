"use strict" ;

import Receiver from '../../../system/signals/Receiver'
import logger from '../../logging/logger'

/**
 * This controller is invoked when a state is removed from the state model.
 * @name RemoveState
 * @class
 * @memberof molecule.states.controllers
 * @constructor
 */
export default function RemoveState() {}

RemoveState.prototype = Object.create( Receiver.prototype ,
{
    constructor : { value : RemoveState } ,

    /**
     * Receive a message from a state model.
     * @name receive
     * @memberof molecule.states.controllers.RemoveState
     * @instance
     * @function
     */
    receive : { value : function ( state )
    {
        logger.debug( this + " receive : " + state ) ;
    }}
});