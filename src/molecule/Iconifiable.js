"use strict" ;

/**
 * This interface defines all display components with an icon.
 * @name Iconifiable
 * @memberof molecule
 * @interface
 */
export default class Iconifiable
{
    constructor()
    {
        Object.defineProperties( this ,
        {
            /**
             * An icon reference.
             * @name icon
             * @memberof molecule.Iconifiable
             * @instance
             */
            icon : { value : null , configurable : true , writable : true }
        }) ;
    }
}