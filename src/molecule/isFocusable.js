/*jshint unused: false*/
"use strict" ;

import Focusable from './Focusable'

import isGroupable from './isGroupable'

/**
 * Indicates if the specific object is Focusable.
 * @function
 * @memberof molecule
 * @param {object} target - The object to evaluate.
 * @return <code>true</code> if the object is {@link molecule.Focusable|Focusable}.
 */
export default function isFocusable( target )
{
    if( target )
    {
        return target instanceof Focusable || ( isGroupable(target) && ( 'selected' in target ) ) ;
    }
    return false ;
}