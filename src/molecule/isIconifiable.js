"use strict" ;

import Iconifiable from './Iconifiable'

/**
 * Indicates if the specific object is Iconifiable.
 * @function
 * @memberof molecule
 * @param {object} target - The object to evaluate.
 * @return <code>true</code> if the object is {@link molecule.Iconifiable|Iconifiable}.
 */
export default function isIconifiable( target )
{
    if( target )
    {
        return target instanceof Iconifiable || ( 'icon' in target ) ;
    }
    return false ;
}