/*jshint unused: false*/
"use strict" ;

import Groupable from './Groupable'

/**
 * Indicates if the specific objet is Groupable.
 * @function
 * @memberof molecule
 * @param {object} target - The object to evaluate.
 * @return <code>true</code> if the object is {@link molecule.Groupable|Groupable}.
 */
export default function isGroupable( target )
{
    if( target )
    {
        return target instanceof Groupable || ( ( 'group' in target ) && ( 'groupName' in target ) ) ;
    }
    return false ;
}