"use strict" ;

import { sayHello } from './core/hello.js'
import ucFirst from './core/strings/ucFirst.js'

import './polyfill/index.js'

/**
 * The VEGAS.js framework.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @version 1.0.19
 */

import { global } from './core/global'

import core     from './core'
import system   from './system'
import graphics from './graphics'
import screens  from './screens'

//export default global ;

export { sayHello , skipHello }  from './core/hello.js' ;

/**
 * The string expression of the current VEGAS version.
 * @name version
 * @type string
 * @global
 */
export const version = '<@VERSION@>' ;

/**
 * The metadatas object to describe the <b>VEGAS JS</b> framework.
 * @name metas
 * @property {string} name - The name of the library
 * @property {string} description - The description of the library
 * @property {string} version - The version of the library
 * @property {string} license - The license of the library
 * @property {string} url - The url of the library
 * @type Object
 * @global
 * @example
 * trace( core.dump( metas ) ) ;
 */
export const metas = Object.defineProperties( {} ,
{
    name        : { enumerable : true , value : ucFirst('<@NAME@>') } ,
    description : { enumerable : true , value : "<@DESCRIPTION@>" },
    version     : { enumerable : true , value : version } ,
    license     : { enumerable : true , value : "<@LICENSE@>" } ,
    url         : { enumerable : true , value : '<@HOMEPAGE@>' }
});

try
{
    if ( window )
    {
        window.addEventListener( 'load' , function load()
        {
            window.removeEventListener( "load", load, false ) ;
            sayHello(metas.name,metas.version,metas.url) ;
        }, false );
    }
}
catch( error )
{
    // do nothing
}

Object.defineProperty( exports , '__esModule', { value: true });

exports.global   = global ;
exports.trace    = core.trace ;
exports.core     = core ;
exports.system   = system ;
exports.graphics = graphics ;
exports.screens  = screens ;
