"use strict" ;

import './polyfill/Object.js' ;

import Enum        from './system/Enum'
import Equatable   from './system/Equatable'
import Evaluable   from './system/Evaluable'
import Formattable from './system/Formattable'

import isEvaluable   from './system/isEvaluable'
import isFormattable from './system/Formattable'

import calendar    from './system/calendar'
import data        from './system/data'
import errors      from './system/errors'
import evaluators  from './system/evaluators'
import events      from './system/events'
import formatters  from './system/formatters'
import ioc         from './system/ioc'
import logging     from './system/logging'
import logics      from './system/logics'
import models      from './system/models'
import numeric     from './system/numeric'
import process     from './system/process'
import rules       from './system/rules'
import signals     from './system/signals'
import transitions from './system/transitions'

/**
 * The {@link system} library is the root package for the <strong>VEGAS JS</strong> framework. It is the starting point of our RIA framework structure : signals, data, IoC, logger, tasks, transitions, logics, rules, models, etc.
 * <p><b>Dependencies :</b> The {@link system} framework reuse the module and building blocks of the {@link core} library.</p>
 * @summary The {@link system} library is the root package for the <strong>VEGAS JS</strong> framework.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace system
 * @version 1.0.19
 * @since 1.0.0
 */
const system = Object.assign
({
    // interfaces

    Enum        : Enum ,
    Equatable   : Equatable ,
    Evaluable   : Evaluable ,
    Formattable : Formattable ,

    // functions

    isEvaluable   : isEvaluable ,
    isFormattable : isFormattable ,

    // packages

    calendar    : calendar,
    data        : data ,
    errors      : errors ,
    evaluators  : evaluators ,
    events      : events ,
    formatters  : formatters ,
    ioc         : ioc ,
    logging     : logging ,
    logics      : logics ,
    models      : models ,
    numeric     : numeric ,
    process     : process ,
    rules       : rules ,
    signals     : signals,
    transitions : transitions
}) ;

export default system ;