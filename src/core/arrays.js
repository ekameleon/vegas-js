"use strict" ;

import '../polyfill/Object.js' ;

import contains    from './arrays/contains'
import initialize  from './arrays/initialize'
import pierce      from './arrays/pierce'
import repeat      from './arrays/repeat'
import rotate      from './arrays/rotate'
import shuffle     from './arrays/shuffle'
import sortOn      from './arrays/sortOn'
import spliceInto  from './arrays/spliceInto'
import swap        from './arrays/swap'

/**
 * The {@link core.arrays} package is a modular <b>JavaScript</b> library that provides extra <code>Array</code> methods.
 * @summary The {@link core.arrays} package is a modular <b>JavaScript</b> library that provides extra <code>Array</code> methods.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace core.arrays
 * @memberof core
 */
const arrays = Object.assign
({
    contains : contains ,
    initialize : initialize ,
    pierce : pierce ,
    repeat : repeat ,
    rotate : rotate ,
    shuffle : shuffle ,
    sortOn : sortOn ,
    spliceInto : spliceInto,
    swap : swap
}) ;

export default arrays ;