"use strict" ;

/**
 * Anti-hyperbolic sine.
 * @name asinH
 * @memberof core.maths
 * @function
 * @instance
 */
const asinH = ( x ) => Math.log( x + Math.sqrt(x * x + 1) );

export default asinH ;