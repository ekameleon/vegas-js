"use strict" ;

/**
 * This constant defines the radius of the earth in meter : <code>6371 km</code>.
 * @name EARTH_RADIUS_IN_METERS
 * @memberof core.maths
 * @instance
 * @const
 */
const EARTH_RADIUS_IN_METERS = 6371000 ;

export default EARTH_RADIUS_IN_METERS;
