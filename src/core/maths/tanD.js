"use strict" ;

import DEG2RAD from './DEG2RAD'

/**
 * Calculates the tangent of the passed angle.
 * @name tanD
 * @memberof core.maths
 * @function
 * @instance
 * @param {number} angle - The angle in degrees.
 * @return The tangent of the passed angle.
 */
const tanD = ( angle ) => Math.tan( angle * DEG2RAD ) ;

export default tanD;