"use strict" ;

import RAD2DEG from './RAD2DEG'

/**
 * Calculates the arctangent2 of the passed angle.
 * @name atan2D
 * @memberof core.maths
 * @function
 * @instance
 * @param {number} y - A value representing y-axis of angle vector.
 * @param {number} x - A value representing x-axis of angle vector.
 * @return the arctangent2 of the passed angle.
 */
const atan2D = ( y , x ) => Math.atan2( y , x ) * RAD2DEG ;

export default atan2D ;