"use strict" ;

import DEG2RAD from './DEG2RAD'

/**
 * Converts degrees to radians.
 * @name degreesToRadians
 * @memberof core.maths
 * @function
 * @instance
 * @param {number} angle - Value, in degrees, to convert to radians.
 * @return The angle in radians.
 */
const degreesToRadians = ( angle ) => angle * DEG2RAD ;

export default degreesToRadians;