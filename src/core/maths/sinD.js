"use strict" ;

import DEG2RAD from './DEG2RAD'

/**
 * Calculates the sine of the passed angle.
 * @name sinD
 * @memberof core.maths
 * @function
 * @instance
 * @param {number} angle - A value in degrees.
 * @return The sine of the passed angle, a number between <code>-1</code> and <code>1</code> inclusive.
 */
const sinD = ( angle ) => Math.sin( angle * DEG2RAD ) ;

export default sinD;