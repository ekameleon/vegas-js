"use strict" ;

/**
 * Determines whether the specified object exists as an element in an Array object.
 * @name contains
 * @memberof core.arrays
 * @function
 * @instance
 * @param {Array} array - The search Array.
 * @param {*} value - The object to find in the array.
 * @return The value <code>true</code> if the specified object exists as an element in the array ; otherwise, <code>false</code>.
 * @example
 * var ar = [2, 3, 4] ;
 * trace( contains( ar , 3 ) ) ; // true
 * trace( contains( ar , 5 ) ) ; // false
 */
const contains = ( array  , value ) =>
{
    return (array instanceof Array) ? (array.indexOf(value) > -1) : false ;
};

export default contains ;