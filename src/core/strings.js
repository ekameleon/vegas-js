"use strict" ;

import '../polyfill/Object'

import between           from './strings/between'
import camelCase         from './strings/camelCase'
import capitalize        from './strings/capitalize'
import center            from './strings/center'
import clean             from './strings/clean'
import compare           from './strings/compare'
import deburr            from './strings/deburr'
import endsWith          from './strings/endsWith'
import fastformat        from './strings/fastformat'
import fastformatDate    from './strings/fastformatDate'
import format            from './strings/format'
import hyphenate         from './strings/hyphenate'
import indexOfAny        from './strings/indexOfAny'
import insert            from './strings/insert'
import lastIndexOfAny    from './strings/lastIndexOfAny'
import pad               from './strings/pad'
import repeat            from './strings/repeat'
import startsWith        from './strings/startsWith'
import trim              from './strings/trim'
import trimEnd           from './strings/trimEnd'
import trimStart         from './strings/trimStart'
import truncate          from './strings/truncate'
import ucFirst           from './strings/ucFirst'
import ucWords           from './strings/ucWords'
import validateUUID      from './strings/validateUUID'
import versionUUID       from './strings/versionUUID'

// import latinize          from './strings/latinize'

/**
 * The {@link core.strings} package is a modular <b>JavaScript</b> library that provides extra <code>String</code> methods.
 * @summary The {@link core.strings} package is a modular <b>JavaScript</b> library that provides extra <code>String</code> methods.
 * @namespace core.strings
 * @memberof core
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 */
const strings = Object.assign
({
    between : between ,
    camelCase : camelCase ,
    capitalize : capitalize ,
    center : center ,
    clean : clean ,
    compare : compare ,
    deburr : deburr ,
    endsWith : endsWith ,
    fastformat : fastformat ,
    fastformatDate : fastformatDate ,
    format : format ,
    hyphenate : hyphenate ,
    indexOfAny : indexOfAny ,
    insert : insert ,
    lastIndexOfAny : lastIndexOfAny ,
    //latinize : latinize ,
    pad : pad ,
    repeat : repeat ,
    startsWith : startsWith ,
    trim : trim ,
    trimEnd : trimEnd ,
    trimStart : trimStart ,
    truncate : truncate ,
    ucFirst : ucFirst ,
    ucWords : ucWords ,
    validateUUID : validateUUID ,
    versionUUID : versionUUID
}) ;

export default strings ;