"use strict" ;

import '../polyfill/Object'

import compare            from './chars/compare'
import isAlpha            from './chars/isAlpha'
import isAlphaOrDigit     from './chars/isAlphaOrDigit'
import isASCII            from './chars/isASCII'
import isContained        from './chars/isContained'
import isDigit            from './chars/isDigit'
import isHexDigit         from './chars/isHexDigit'
import isIdentifierStart  from './chars/isIdentifierStart'
import isLineTerminator   from './chars/isLineTerminator'
import isLower            from './chars/isLower'
import isOctalDigit       from './chars/isOctalDigit'
import isOperator         from './chars/isOperator'
import isSymbol           from './chars/isSymbol'
import isUnicode          from './chars/isUnicode'
import isUpper            from './chars/isUpper'
import isWhiteSpace       from './chars/isWhiteSpace'
import lineTerminators    from './chars/lineTerminators'
import operators          from './chars/operators'
import symbols            from './chars/symbols'
import whiteSpaces        from './chars/whiteSpaces'

/**
 * The {@link core.chars} package is a modular <b>JavaScript</b> library that provides extra <code>String</code> methods to validate and transform the basic characters.
 * <p>You can use this library for example to parse a string (JSON, csv, etc.).</p>
 * @summary The {@link core.chars} package is a modular <b>JavaScript</b> library that provides extra <code>String</code> methods to validate and transform a basic character.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace core.chars
 * @memberof core
 */
const chars = Object.assign
({
    compare           : compare ,
    isAlpha           : isAlpha ,
    isAlphaOrDigit    : isAlphaOrDigit ,
    isASCII           : isASCII ,
    isContained       : isContained ,
    isDigit           : isDigit ,
    isHexDigit        : isHexDigit ,
    isIdentifierStart : isIdentifierStart ,
    isLineTerminator  : isLineTerminator ,
    isLower           : isLower ,
    isOctalDigit      : isOctalDigit ,
    isOperator        : isOperator ,
    isSymbol          : isSymbol ,
    isUnicode         : isUnicode ,
    isUpper           : isUpper ,
    isWhiteSpace      : isWhiteSpace ,
    lineTerminators   : lineTerminators ,
    operators         : operators ,
    symbols           : symbols ,
    whiteSpaces       : whiteSpaces
}) ;

export default chars ;