"use strict" ;

/**
 * Dumps a string representation of any Date reference.
 * @name dumpDate
 * @memberof core
 * @function
 * @instance
 * @param {Date} date - A Date object to dump.
 * @param {boolean} [timestamp=false] - The optional timestamp flag.
 * @return The string representation of any Date reference.
 */
export default function dumpDate( date /*Date*/ , timestamp = false  )
{
    timestamp = Boolean( timestamp ) ;
    if ( timestamp )
    {
        return "new Date(" + String( date.valueOf() ) + ")";
    }
    else
    {
        let y    = date.getFullYear();
        let m    = date.getMonth();
        let d    = date.getDate();
        let h    = date.getHours();
        let mn   = date.getMinutes();
        let s    = date.getSeconds();
        let ms   = date.getMilliseconds();
        let data = [ y, m, d, h, mn, s, ms ];
        data.reverse();
        while( data[0] === 0 )
        {
            data.splice( 0, 1 );
        }
        data.reverse() ;
        return "new Date(" + data.join( "," ) + ")";
    }
}