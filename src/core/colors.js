"use strict" ;

import '../polyfill/Object'

import distance  from './colors/distance'
import equals    from './colors/equals'
import fade      from './colors/fade'
import fromARGB  from './colors/fromARGB'
import getAlpha  from './colors/getAlpha'
import getBlue   from './colors/getBlue'
import getGreen  from './colors/getGreen'
import getRed    from './colors/getRed'
import hex       from './colors/hex'
import isUnique  from './colors/isUnique'
import toHex     from './colors/toHex'
import uniques   from './colors/uniques'

/**
 * The {@link core.colors} package is a modular <b>JavaScript</b> library that provides extra <b>rgb color</b> methods.
 * @summary The {@link core.colors} package is a modular <b>JavaScript</b> library that provides extra <b>rgb color</b> methods.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace core.colors
 * @memberof core
 */
const colors = Object.assign
({
    distance : distance ,
    equals   : equals ,
    fade     : fade ,
    fromARGB : fromARGB ,
    getAlpha : getAlpha ,
    getBlue  : getBlue ,
    getGreen : getGreen ,
    getRed   : getRed ,
    hex      : hex,
    isUnique : isUnique ,
    toHex    : toHex ,
    uniques  : uniques
}) ;

export default colors ;