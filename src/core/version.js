export default class version
{
    /**
     * Creates a new version instance.
     */
    constructor( major = 0, minor = 0, build = 0, revision = 0 )
    {
        this._value = (major << 28) | (minor << 24) | (build << 16) | revision ;
    }
    
    /**
     * Indicates the build value of this version.
     */
    get build()
    {
        return (this._value & 0x00FF0000) >>> 16;
    }
    
    /**
     * @private
     */
    set build( value )
    {
        this._value = (this._value & 0xFF00FFFF) | (value << 16);
    }
    
    /**
     * Indicates the major value of this version.
     */
    get major()
    {
        return this._value >>> 28;
    }
    
    /**
     * @private
     */
    set major( value )
    {
        this._value = (this._value & 0x0FFFFFFF) | (value << 28);
    }
    
    /**
     * Indicates the minor value of this version.
     */
    get minor()
    {
        return (this._value & 0x0F000000) >>> 24;
    }
    
    /**
     * @private
     */
    set minor( value )
    {
        this._value = (this._value & 0xF0FFFFFF) | (value << 24);
    }
    
    /**
     * Indicates the revision value of this version.
     */
    get revision()
    {
        return this._value & 0x0000FFFF;
    }
    
    /**
     * @private
     */
    set revision( value )
    {
        this._value = ( this._value & 0xFFFF0000) | value;
    }
    
    /**
     * Constructs a version object from a number.
     * If the number is zero or negative, or is NaN or Infity returns an empty version object.
     */
    static fromNumber( value = 0 )
    {
        let v = new version();
        
        if( isNaN( value ) || (value == 0) || (value < 0) || (value == Number.MAX_VALUE) || (value == Number.POSITIVE_INFINITY) || (value == Number.NEGATIVE_INFINITY) )
        {
            return v;
        }
        
        v.major    = (value >>> 28);
        v.minor    = (value & 0x0f000000) >>> 24;
        v.build    = (value & 0x00ff0000) >>> 16;
        v.revision = (value & 0x0000ffff);
        
        return v;
    }
    
    /**
     * Constructs a version object from a string.
     */
    static fromString( value = "", separator = "." )
    {
        var v = new version();
        
        if( (value == null) || !(value instanceof String || typeof(value) === 'string' ) || (value == "") )
        {
            return v ;
        }
        
        if( value.indexOf( separator ) > - 1 )
        {
            let values = value.split( separator );
            let len    = values.length ;
            
            if( len > 0 )
            {
                v.major = parseInt( values[0] ) ;
            }
     
            if( len > 1 )
            {
                v.minor = parseInt( values[1] ) ;
            }
    
            if( len > 2 )
            {
                v.build = parseInt( values[2] ) ;
            }
    
            if( len > 3 )
            {
                v.revision = parseInt( values[3] ) ;
            }
        }
        else
        {
            v.major = parseInt( value );
        }
        
        return v;
    }
    
    toString( fields = 0, separator = "." )
    {
        let data = [this.major,this.minor,this.build,this.revision];
        if( (fields > 0) && (fields < 5) )
        {
            data = data.slice( 0, fields );
        }
        else
        {
            let i = 0;
            let l = data.length;
            for( i=l-1 ; i>0 ; i-- )
            {
                if( data[i] == 0 )
                {
                    data.pop();
                }
                else
                {
                    break;
                }
            }
        }
        
        return data.join( separator );
    }
    
    /**
     * Returns the primitive value of the object.
     * @return the primitive value of the object.
     */
    valueOf()
    {
        return this._value;
    }
}