"use strict" ;

import './polyfill/Object'

import isGroupable   from './molecule/isGroupable'
import isFocusable   from './molecule/isFocusable'
import isIconifiable from './molecule/isIconifiable'

import Builder         from './molecule/Builder'
import Deployment      from './molecule/Deployment'
import Focusable       from './molecule/Focusable'
import Groupable       from './molecule/Groupable'
import Iconifiable     from './molecule/Iconifiable'
import IconPolicy      from './molecule/IconPolicy'
import InteractiveMode from './molecule/InteractiveMode'
import LabelPolicy     from './molecule/LabelPolicy'
import ScrollPolicy    from './molecule/ScrollPolicy'
import Style           from './molecule/Style'

import components from './molecule/components'
import display    from './molecule/display'
import groups     from './molecule/groups'
import logger     from './molecule/logging/logger'
import render     from './molecule/render'
import states     from './molecule/states'

/**
 * The {@link molecule} package is a library for develop crossplatform Rich Internet Applications and Games.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace molecule
 * @version 1.0.19
 * @since 1.0.8
 */
const molecule = Object.assign
({
    // ----- Singletons

    logger,
    
    // ----- functions
    
    isGroupable   : isGroupable,
    isFocusable   : isFocusable,
    isIconifiable : isIconifiable,

    // ----- Classes and enumerations

    Builder         : Builder ,
    Deployment      : Deployment ,
    Focusable       : Focusable ,
    Groupable       : Groupable ,
    Iconifiable     : Iconifiable ,
    IconPolicy      : IconPolicy ,
    InteractiveMode : InteractiveMode ,
    LabelPolicy     : LabelPolicy ,
    ScrollPolicy    : ScrollPolicy,
    Style           : Style,

    // ----- packages

    components : components,
    display    : display,
    groups     : groups,
    render     : render,
    states     : states
}) ;

export default molecule ;